# Copyright 2021 Rafał Safin (rafsaf). All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import datetime

from django.contrib.auth.models import User
from django.test import TestCase

from base import models
from utils.basic import TargetWeightQueries


class TestTargetWeightQueries(TestCase):
    def setUp(self):
        self.server = models.Server.objects.create(
            dns="testserver",
            prefix="te",
        )
        self.world1 = models.World.objects.create(
            server=self.server,
            postfix="1",
            paladin="inactive",
            archer="inactive",
            militia="active",
        )
        self.admin = User.objects.create_user("admin", None, None)  # type: ignore
        self.outline = models.Outline.objects.create(
            owner=self.admin,
            date=datetime.date.today(),
            name="name",
            world=self.world1,
            ally_tribe_tag=["pl1", "pl2"],
            enemy_tribe_tag=["pl3", " pl4"],
        )

        self.state = models.WeightMaximum.objects.create(
            outline=self.outline,
            start="100|100",
            player="player1",
            off_max=100,
            off_state=100,
            off_left=0,
            nobleman_max=2,
            nobleman_state=2,
            nobleman_left=2,
        )
        self.outline_time = models.OutlineTime.objects.create(outline=self.outline)

        self.period = models.PeriodModel.objects.create(
            status="all",
            outline_time=self.outline_time,
            unit="ram",
        )

        self.target = models.TargetVertex.objects.create(
            outline=self.outline,
            player="player2",
            target="101|101",
            outline_time=self.outline_time,
        )
        self.weight = models.WeightModel.objects.create(
            start="100|100",
            player="player1",
            target=self.target,
            state=self.state,
            distance=1,
            off=20,
            nobleman=1,
            order=1,
        )
        self.target_query: TargetWeightQueries = TargetWeightQueries(self.outline)
        self.targets = list(self.target_query.targets)

    def test____weights(self):
        with self.assertNumQueries(1):
            list(self.target_query._TargetWeightQueries__weights())  # type: ignore

    def test___time_periods(self):
        with self.assertNumQueries(1):
            list(self.target_query._TargetWeightQueries__time_periods())  # type: ignore

    def test_target_period_dictionary(self):
        with self.assertNumQueries(1):
            self.target_query.target_period_dictionary()

    def test___dict_with_village_ids(self):
        with self.assertNumQueries(1):
            self.target_query._TargetWeightQueries__dict_with_village_ids([0])  # type: ignore

    def test___target_dict_with_weights_extendeds(self):
        with self.assertNumQueries(1):
            self.target_query.target_dict_with_weights_extended()

    def test_target_dict_read_correct_dictionary(self):
        dict_true = {}
        weight = self.weight
        weight.distance = "0.0"
        dict_true[self.target] = [weight]
        self.assertEqual(self.target_query.target_dict_with_weights_read(), dict_true)

    def test_target_dict_read_correct_number_of_queries(self):
        with self.assertNumQueries(1):
            self.target_query.target_dict_with_weights_read()

    def test___create_target_dict_correct_dict(self):
        dict_result = {}
        dict_result[self.target] = []
        self.assertEqual(
            dict_result,
            self.target_query._TargetWeightQueries__create_target_dict(),  # type: ignore
        )
